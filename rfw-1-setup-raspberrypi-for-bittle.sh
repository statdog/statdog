# This "run-from-web" code can be downloaded and run with this one line script:

# wget https://gitlab.com/statdog/statdog/-/raw/main/rfw-1-setup-raspberrypi-for-bittle.sh -O /tmp/setup.sh ; bash /tmp/setup.sh [Optional_new_hostname]


echo "Test with:"
echo "  - Petoi BiBoard V1"
echo "  - Petoi Nyboard"
echo "  - Raspberry Pi 5"
echo "  - Raspberry Pi OS Lite (64-bit) Released 2024-11-19 (with or without apt-update upgrade as of 2025-03-01)"
echo "  - Python 3.11.2 (results of current version install on 2025-03-01)"

#set -xv
sudo apt-get update
if [[ -z "$(command -v python3)" ]] ; then sudo apt-get install -y python3; fi
if [[ -z "$(command -v git)" ]] ; then sudo apt-get install -y git; fi

sudo apt-get install -y python3-tk python3-translate python3-serial micro

if [[ ! -f /petoi/serialMaster/ardSerial.py ]]; then
  PETOIREPO=https://github.com/PetoiCamp/OpenCat.git
  REFTOPULL=main
  mkdir -p petoi
  pushd petoi
  git init
  git remote add -f origin ${PETOIREPO}
  git config core.sparseCheckout true
  echo "serialMaster/" >> .git/info/sparse-checkout
  git pull origin ${REFTOPULL}

  curl -o serialMaster/translate.py https://raw.githubusercontent.com/PetoiCamp/OpenCat/refs/heads/main/pyUI/translate.py  

  echo "serial commands are in $(pwd)/serialMaster/"
  popd
fi

if [[ -n "$1" ]] && [[ "$(hostname)" != "$1" ]]; then
  NEWHOSTNAME=$1
  sudo hostnamectl set-hostname $NEWHOSTNAME
  sudo sed -i "s/raspberrypi/$NEWHOSTNAME/g" /etc/hosts
fi

if [[ $(grep "^dtparam=i2c_arm=off" /boot/firmware/config.txt -c) -lt 1 ]]; then
  echo '# Force i2c off for self-documentation of requirement' | sudo tee -a /boot/firmware/config.txt 
  echo 'dtparam=i2c_arm=off' | sudo tee -a /boot/firmware/config.txt
fi

if [[ $(grep "^dtparam=spi=off" /boot/firmware/config.txt -c) -lt 1 ]]; then
  echo '# Force spi off for self-documentation of requirement' | sudo tee -a /boot/firmware/config.txt 
  echo 'dtparam=spi=off' | sudo tee -a /boot/firmware/config.txt
fi

echo "The next two updates are equivalent to configuring the serial port using raspi-config described here: https://bittle.petoi.com/4-configuration#4-4-1-config-raspberry-pi-serial-port"
if [[ $(grep "console=serial0,115200" /boot/firmware/cmdline.txt -c) -ge 1 ]]; then
  echo "Disable console on serial"
  sudo sed -i "s/console=serial0,115200 //g" /boot/firmware/cmdline.txt
fi

if [[ $(grep "^enable_uart=1" /boot/firmware/config.txt -c) -lt 1 ]]; then
  echo "Enable Serial hardware"
  echo 'enable_uart=1' | sudo tee -a /boot/firmware/config.txt
fi

# needed on Nyboard
if [[ $(grep "^dtparam=uart0=on" /boot/firmware/config.txt -c) -lt 1 ]]; then
  echo "#===>>> Enable uart0" | sudo tee -a /boot/firmware/config.txt 
  echo 'dtparam=uart0=on' | sudo tee -a /boot/firmware/config.txt
fi

if [[ $(grep "dtoverlay=w1-gpio" /boot/firmware/config.txt -c) -ge 1 ]]; then
  echo "Removing 1-Wire Configuration as required in: https://bittle.petoi.com/4-configuration#4-4-1-config-raspberry-pi-serial-port"
  sudo sed -i "s/dtoverlay=w1-gpio//g" /boot/firmware/config.txt
fi

if [[ $(grep "cgroup_enable=cpuset" /boot/firmware/cmdline.txt -c) -lt 1 ]]; then
  echo "#===>>> Adding cgroup settings for kubernetes (k3s)"
  sudo sed -i "s/rootwait/rootwait cgroup_enable=cpuset cgroup_enable=memory cgroup_memory=1/g" /boot/firmware/cmdline.txt
fi

#For RPI 2,3 or 4
if grep -q "Raspberry Pi [234]" /proc/device-tree/model; then
  if [[ $(grep "^dtoverlay=miniuart-bt" /boot/config.txt -c) -lt 1 ]]; then
    echo '# Reassign BT UART to Mini Uart, but keep bt working' | sudo tee -a /boot/config.txt
    echo 'dtoverlay=miniuart-bt' | sudo tee -a /boot/config.txt
  fi  
fi

echo "#===>>> Serial port reconfiguration for Ubuntu, which appears required for Raspberry Pi OS as well, documented here: https://docs.petoi.com/?q=raspberry+pi"
sudo systemctl stop serial-getty@ttyS0.service
sudo systemctl disable serial-getty@ttyS0.service

if [[ ! -f /etc/udev/rules.d/50-tty.rules ]]; then
  if grep -q "Raspberry Pi [234]" /proc/device-tree/model; then
    echo 'KERNEL=="ttyS0", SYMLINK+="serial0" GROUP="tty" MODE="0660"' | sudo tee -a /etc/udev/rules.d/50-tty.rules
  else
    #Pi 5 or later
    echo 'KERNEL=="ttyAMA10", SYMLINK+="serial1" GROUP="tty" MODE="0660"' | sudo tee -a /etc/udev/rules.d/50-tty.rules
  fi
  echo 'KERNEL=="ttyAMA0", SYMLINK+="serial1" GROUP="tty" MODE="0660"' | sudo tee -a /etc/udev/rules.d/50-tty.rules
fi

if [[ -f /dev/ttyS0 ]]; then sudo chmod g+r /dev/ttyS0; fi
if [[ -f /dev/ttyAMA0 ]]; then sudo chmod g+r /dev/ttyAMA0; fi 
if [[ -f /dev/ttyAMA10 ]]; then sudo chmod g+r /dev/ttyAMA10; fi 

sudo udevadm control --reload-rules
sudo udevadm trigger
#sudo chgrp -h tty /dev/serial0
#sudo chgrp -h tty /dev/serial1
sudo adduser $USER tty
sudo adduser $USER dialout

if [ -n "$OSUPDATE" ]; then sudo apt-get -y upgrade; fi

echo "Use the following command to test for success after the reboot 'python3 ardSerial.py kbalance' Bittle should perform the balance instinct and move into a standing position."
echo .
read -n 1 -s -r -p "Press any key to reboot..."

sudo reboot now
