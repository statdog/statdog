### Overview

Using as much automation code as possible, this guide will enable you to:
1. Configure a Raspberry Pi 3A+ / 4 / 5 to interface with the Petoi Bittle robot which is Ardruino based.
2. Install and Configure GitLab Runner (Optional)

So that you can have Bittle respond to GitLab CI/CD Pipelines with audio and robotic actions!

### Tested as working with:
- Petoi BiBoard V1
- Petoi Nyboard
- Raspberry Pi 5
- Raspberry Pi OS Lite (64-bit) Released 2024-11-19 (with or without apt-update upgrade as of 2025-03-01)
- Python 3.11.2 (results of current version install on 2025-03-01)

### Bittle NyBoard: Solder a 2 x 5 Socket

> Note: all the board configuration and software configuration steps mentioned in the following guide are performed by the instructions and code that follow - this link is provided for you to verify that the 2x5 socket must be soldered onto Bittle. After the soldering the back will no longer click into place even if the Pi is not attached. I have reattached the back using velcro tape on the corners of the Pi.

- [ ] [Instructions on Petoi Site](https://bittle.petoi.com/4-configuration#4.4.-raspberry-pi-serial-port-as-an-interface)

### Bittle BiBoard V1: Solder a 1 x 5 Socket

- [ ] [Instructions on Petoi Site](https://docs.petoi.com/apis/raspberry-pi-serial-port-as-an-interface/for-biboard-v1) - ignore the instructions about editing ardSerial.py, this code no longer exists in the latest version
- [ ] After any power cycle, you must use the serial monitor interface (usually in ardruino IDE) to disable (send "X") and re-enable (send "XS") the GPIO interface on the board. **IMPORTANT**: The setting persists across powercycles, but it does not actually enable the interface on powerup.

### Burn the SD

See "Tested as working with" section above for what OS and version to use.

### Configure the Pi for AutoLogin to Wireless with SSH Enabled

The [Raspberry PI Imaging Tool v1.8.5 and later](https://www.raspberrypi.com/software/) now supports doing all of these settings during the SD card image transfer process including enabling ssh and wifi.

### Configure Pi To Control Bittle

#### Running The Configuration Script From a URL
Using a direct "run from web" script, the following updates your Pi to:

- the latest which is required for bluetooth sound to work reliably - but it is also just a good idea in general.
- the latest os packages
- rename the hostname to the name you provide (optional but recommended)
- updates raspi-config settings required by bittle in https://bittle.petoi.com/4-configuration#4-4-1-config-raspberry-pi-serial-port

Compare Your Version: `cat /etc/rpi-issue` To The Release Notes: <https://downloads.raspberrypi.org/raspios_full_armhf/release_notes.txt>

- [ ] Login to the Pi
  - If your router supports .local dns and if your ssh machine is on the same network, then try sshing to "pi@actual_rpi_hostname.local", 
  - if that does not work, you'll need tologin to your router and find the Pis ip address in your router's dhcp client list.
- [ ] Login with the default user "pi" and password "raspberry" using (The extra settings help if you end up imaging the SD a lot and it get's new host keys every time) `ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null pi@123.123.123.123`
- [ ] Update this command line to substitute your chosen host name for `[optional_new_hostname]` and then run it:
   `wget https://gitlab.com/statdog/statdog/-/raw/main/rfw-1-setup-raspberrypi-for-bittle.sh -O /tmp/setup.sh ; bash /tmp/setup.sh [Optional_new_hostname]`
- [ ] You will be prompted to change the password for the 'pi' profile near the end and then the system will reboot

#### Testing Operation

- [ ] Boot the prepared SD card while the Pi is attached to the Bittle
- [ ] Login with the default user "pi" and the new password you set in the previous steps using `ssh pi@123.123.123.123`
- [ ] From the /home/pi folder where you will be placed upon logon, run `python3 ardSerial.py kbalance` and Bittle should perform the balance instinct and move into a standing position.

### Configure Bittle to Run GitLab Runner To Be Controlled From GitLab

### On-Premise Device Cloud

The [On-Premise Device Cloud Runner Registration Script Could Also Be Used](https://gitlab.com/guided-explorations/embedded/ci-components/device-cloud/-/blob/main/Device-Cloud-Pool-Mgmt/PROVISIONDEVICEGWRUNNER.sh?ref_type=heads) - **IMPORTANTLY** it includes docker tweaks that allow hardware access from a container. Shell runners is the easiest to get started with.

#### Collect Data

Your GitLab URL and a Runner Registration Token are needed as input to the Run-From-Web oneline script execution.

GitLab runners can be attached to an entire instance, directly to any group or directly to any project.  [Read here about how to find these tokens](https://docs.gitlab.com/runner/register/#requirements).  They are security tokens and should be treated with the same care as passwords.

#### Running Script
Using a direct "run from web" script, the following updates your Pi to:

- install the latest gitlab-runner.
- use the runner tag "petoi-bittle" to ensure only jobs intended for Bittle run on it.
- give the runner user permissions to the serial port, to control bluetooth and to the "pi" group to use the Pi user's permissions.

- [ ] Update this command line to substitute your GitLab instance URL for `[optional_gitlab_instance_url]` and your runner registration token for `[optional_gitlab_runner_reg_token]` then run it:
   `wget https://gitlab.com/statdog/statdog/-/raw/main/rfw-2-setup-gitlab-runner.sh -O /tmp/setuprunner.sh ; sudo bash /tmp/setuprunner.sh [optional_gitlab_instance_url] [optional_gitlab_runner_reg_token]`
- [ ] Use the following .gitlab-ci.yml in a project that the bittle runner is registered to, to test that the runner is working
    ```
    test-job:
      tags:
        - petoi-bittle    
      script:
        - |
          echo "Success!"
          python3 ardSerial.py ksit
   ```
