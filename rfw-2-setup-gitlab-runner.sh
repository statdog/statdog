
# This "run-from-web" code can be downloaded and run with this one line script:

# wget https://gitlab.com/statdog/statdog/-/raw/main/rfw-2-setup-gitlab-runner.sh -O /tmp/setuprunner.sh ; sudo bash /tmp/setuprunner.sh [optional_gitlab_instance_url] [optional_gitlab_runner_reg_token]

if [[ "$(uname -m)" == "aarch64" ]]; then echo "Installing for 64bit arm"; runnerarch=arm64; else echo "Installing for 32bit arm"; runnerarch=arm; fi

if [[ -z "$(command -v git)" ]] ; then apt-get update; apt-get install -y git; fi

if [ $# -eq 2 ]; then
  if [[ ! -f /usr/bin/gitlab-runner ]]; then
    echo "Installing the latest version..."
    sudo gitlab-runner stop
    sudo curl -L --output /usr/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-${runnerarch}"
    sudo chmod +x /usr/bin/gitlab-runner
    sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
    if [[ -f /home/gitlab-runner/.bash_logout ]]; then sudo mv /home/gitlab-runner/.bash_logout /home/gitlab-runner/bash_logout-backup; fi
    sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
    
    echo "Giving user 'gitlab-runner' permissions to use the serial port and bluetooth..."
    sudo usermod -G pi -a gitlab-runner #allow SU to pi
    sudo usermod -G dialout -a gitlab-runner #allow runner to access serial

    sudo service gitlab-runner start
    echo "Granting passwordless sudo for interfacing with hardware in CI jobs"
    echo "gitlab-runner ALL=(ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers.d/gitlab-runner

    #sudo usermod -a -G sudo gitlab-runner
    
    #sudo echo '%gitlab-runner ALL=(ALL) NOPASSWD: ALL' | sudo tee -a /etc/sudoers.d/gitlab-runner
    #sudo chown root:root /etc/sudoers.d/gitlab-runner
    #sudo chmod 0644 /etc/sudoers.d/gitlab-runner
  fi
  sudo gitlab-runner unregister --all-runners
  sudo gitlab-runner register --non-interactive --name "$(hostname)" -executor "shell" --url $1 --token $2
  echo "Use the following .gitlab-ci.yml to test for success after the reboot 
    test-job:
      tags:
        - petoi-bittle    
      script:
        - |
          echo 'Success!'
          python3 ardSerial.py kbalance
   Bittle should perform the balance instinct and move into a standing position."
  echo .
  read -n 1 -s -r -p "Press any key to continue..."  
else
  echo "You must provide the target gitlab url and the runner token."
fi

